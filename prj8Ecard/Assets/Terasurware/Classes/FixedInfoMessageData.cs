using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class FixedInfoMessageData : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public string define;
		public string message;
	}
}
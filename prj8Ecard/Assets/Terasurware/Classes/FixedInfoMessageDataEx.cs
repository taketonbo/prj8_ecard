using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class FixedInfoMessageData : ScriptableObject
{	
	/// <summary>
	/// Defineからメッセージを検索.
	/// </summary>
	/// <returns>The from message.</returns>
	/// <param name="i_Define">I_ define.</param>
	public string findFromMessage(string i_Define){
		foreach(Param p in param){
			if(p.define == i_Define){
				return p.message;
			}
		}
		throw new UnityException("Define is Not Found : DefineName >"+i_Define);
	}
}
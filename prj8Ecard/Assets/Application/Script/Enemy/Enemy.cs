using UnityEngine;
using System.Collections;

public class Enemy : UserBase {

	private EnemyCpu m_Cpu;
	public EnemyCpu cpu {get{return m_Cpu;}}

	public GameObject m_FieldSetCardObj;

	protected override void Awake ()
	{
		base.Awake ();
		m_Cpu = gameObject.GetComponent<EnemyCpu>();
		m_FieldSetCardObj = GameObject.Find("EnemySetCard");
	}

	public override void cardSetUp ()
	{
		base.cardSetUp ();
		offCollider();
	}

	private void hideCard(){
		for(int i=0; i < m_CardList.Length; ++i){
			m_CardList[i].cardSprite.spriteName = "back";
		}
	}

	private void rotateCard(){
		for(int i=0; i < m_CardList.Length; ++i){
			Vector3 vec = new Vector3(0,0,180);
			m_CardList[i].transform.eulerAngles = vec;
		}
	}

	private void offCollider(){
		for(int i=0; i < m_CardList.Length; ++i){
			m_CardList[i].buttonCollider.enabled = false;
		}
	}

	public void setCardAnimation(int i_CardIndex){
		cardList[i_CardIndex].moveAnimation(m_FieldSetCardObj);
		cardList[i_CardIndex].scaleAnimation(m_FieldSetCardObj);
	}

	protected override void createCardEmperor ()
	{
		base.createCardEmperor ();
		rotateCard();
		hideCard();
	}

	protected override void createCardSlave ()
	{
		base.createCardSlave ();
		rotateCard();
		hideCard();
	}
}

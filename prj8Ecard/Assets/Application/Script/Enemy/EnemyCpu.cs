﻿using UnityEngine;
using System.Collections;

public class EnemyCpu : MonoBehaviour {
	private Enemy m_MyData;
	private Field m_FieldData;
	void Awake(){
		m_MyData = gameObject.GetComponent<Enemy>();
	}

	void Start(){
		m_FieldData = Field.instance;
	}

	/// <summary>
	/// カードセット処理.
	/// </summary>
	/// <returns>The set card.</returns>
	public IEnumerator selectSetCard(){
		while(true){
			int value = UnityEngine.Random.Range(0,5);
			CardBase card = m_MyData.cardList[value];
			if(card != null){
				// カードセット.
				m_FieldData.enemeySetCard = card;
				// アニメーション処理.
				m_MyData.setCardAnimation(value);
				yield break;
			}
			yield return 0;
		}
	}

	/// <summary>
	/// サイド選択処理.
	/// </summary>
	public void selectSide(){
		GlobalDefine.SIDE_TYPE playerSide = m_FieldData.player.sideType;
		switch(playerSide){
		case GlobalDefine.SIDE_TYPE.EMPEROR:
			m_FieldData.enemy.sideType = GlobalDefine.SIDE_TYPE.SLAVE;
			break;
		case GlobalDefine.SIDE_TYPE.SLAVE:
			m_FieldData.enemy.sideType = GlobalDefine.SIDE_TYPE.EMPEROR;
			break;
		}
	}
}

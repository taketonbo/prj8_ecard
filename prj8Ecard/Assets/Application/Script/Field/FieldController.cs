﻿using UnityEngine;
using System.Collections;

/// <summary>
/// フィールドコントローラ.
/// </summary>
public class FieldController : MonoBehaviour {

	// サイド(皇帝か奴隷か)選択フェイズ.
	public SelectSidePhase m_SideSelectPhase;
	// カード配布フェイズ.
	public CardDistributePhase m_DistributePhase;
	// カードセットフェイズ.
	public CardSetPhase m_SetPhase;
	// カード開示フェイズ.
	public CardOpenPhase m_OpenPhase;
	// カード判定フェイズ.
	public JudgePhase m_JudgePhase;

	void Awake(){
		Field field = Field.instance;

		// インスタンスの取得.
		field.player = GameObject.Find("PlayerCard").GetComponent<Player>();
		field.enemy = GameObject.Find("EnemyCard").GetComponent<Enemy>();
	}

	void Start(){
		// フェイズ開始.
		StartCoroutine(phase());
	}

	/// <summary>
	/// メインフェイズ
	/// </summary>
	private IEnumerator phase(){
		// 皇帝側か奴隷側の選択フェイズ.
		yield return StartCoroutine(m_SideSelectPhase.run());
		// カード配布フェイズ.
		yield return StartCoroutine(m_DistributePhase.run());

		while(true){
			// カードセットフェイズ.
			yield return StartCoroutine(m_SetPhase.run());
			// カード開示フェイズ.
			yield return StartCoroutine(m_OpenPhase.run());
			// カード判定フェイズ.
			yield return StartCoroutine(m_JudgePhase.run());

			if(Field.instance.isGameSet){
				break;
			}
		}
		moveToLoby();
	}

	private void moveToLoby(){
		Application.LoadLevel("Loby");
	}
}

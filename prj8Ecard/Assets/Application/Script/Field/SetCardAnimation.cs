﻿using UnityEngine;
using System.Collections;

public class SetCardAnimation : MonoBehaviour {
	public UISprite m_Sprite;

	private float m_Alpha;
	private bool isTurn;
	private readonly float MAX_ALPHA_VALUE = 0.5f;
	/// <summary>
	/// アニメーション画像の表示.
	/// </summary>
	public void show(){
		initialize();
		m_Sprite.enabled = true;
		StartCoroutine(play());
	}

	/// <summary>
	/// アニメーション画像の非表示.
	/// </summary>
	public void hide(){
		m_Sprite.enabled = false;
	}

	/// <summary>
	/// アニメーション処理.
	/// </summary>
	private IEnumerator play(){
		while(true){
			if(!m_Sprite.enabled){
				break;
			}

			if(isTurn){
				m_Alpha -= Time.deltaTime;
				if(m_Alpha < 0 ){
					isTurn = false;
				}
			}else{
				m_Alpha += Time.deltaTime;
				if(m_Alpha > MAX_ALPHA_VALUE ){
					isTurn = true;
				}
			}
			// 加算.
			m_Sprite.alpha  = m_Alpha;
			yield return 0;
		}
		yield return 0;
	}

	private void initialize(){
		m_Alpha = 0f;
		m_Sprite.alpha = 0f;
		isTurn = false;
	}
}

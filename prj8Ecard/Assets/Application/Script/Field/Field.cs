﻿using UnityEngine;
using System.Collections;

/// <summary>
/// フィールドデータ.
/// </summary>
public class Field : SingletonMono<Field> {
	// userData.
	// enemyData.
	// judge.
	// setCardInfo.

	private bool m_IsGameSet;
	public bool isGameSet{
		get{return m_IsGameSet;}
		set{m_IsGameSet = value;}
	}

	private Player m_Player;
	public Player player{
		get{return m_Player;}
		set{m_Player = value;}
	}

	private Enemy m_Enemy;
	public Enemy enemy {
		get{return m_Enemy;}
		set{m_Enemy = value;}
	}

	// プレイヤー側のセットされているカード.
	private CardBase m_PlayerSetCard;
	public CardBase playerSetCard{
		get{return m_PlayerSetCard;}
		set{ m_PlayerSetCard = value;}
	}

	// エネミー側のセットされているカード.
	private CardBase m_EnemySetCard;
	public CardBase enemeySetCard{
		get{return m_EnemySetCard;}
		set{m_EnemySetCard = value;}
	}


	/// <summary>
	/// フィールド初期化処理.
	/// </summary>
	public void initialize(){
		playerSetCard = null;
		enemeySetCard = null;
	}
}

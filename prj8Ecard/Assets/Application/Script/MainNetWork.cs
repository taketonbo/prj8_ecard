﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class MainNetWork : Photon.MonoBehaviour {

	void Update(){
		if(PhotonNetwork.connectionStateDetailed != PeerState.Joined){
			return;
		}
		this.photonView.RPC("isStart", PhotonTargets.All,false);
	}

	[RPC]
	private void isStart(bool isStart, PhotonMessageInfo messageInfo){
		if(photonView.isMine){
			return;
		}
		// 実処理.
		Debug.Log(messageInfo.sender.name);
	}

}

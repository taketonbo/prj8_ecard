﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 固定データ(Excel).
/// </summary>
public class FixedData : SingletonMono<FixedData> {
	public FixedInfoMessageData m_InfoMessageData;
	public FixedInfoMessageData infoMessageData{get{return m_InfoMessageData;}} 
}

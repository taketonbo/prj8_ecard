﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 定義クラス.
/// </summary>
public class GlobalDefine : MonoBehaviour {
	public static readonly string	SAVE_DATA_NAME = "savedata.bin";
	public static readonly int CARD_MAX_NUM = 5;

	public static string saveDataPath{
		get{
			return Application.persistentDataPath+"/"+SAVE_DATA_NAME;
		}
	}

	public enum CARD_TYPE{
		NONE = 0,
		CITIZEN,	// 市民.
		EMPEROR,	// 皇帝.
		SLAVE		// 奴隷.
	}

	public enum SIDE_TYPE{
		NONE = 0,
		EMPEROR,	// 皇帝側.
		SLAVE		// 奴隷側.
	}
}

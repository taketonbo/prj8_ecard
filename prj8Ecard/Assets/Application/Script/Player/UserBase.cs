﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ユーザーベースクラス.
/// </summary>
public class UserBase : MonoBehaviour {

	protected CardBase[] m_CardList;
	public CardBase[] cardList{get{return m_CardList;}}

	public CardBase m_CitizenClone;	// 市民カードのクローン.
	public CardBase m_EmperorClone;	// 皇帝カードのクローン.
	public CardBase m_SlaveClone;	// 奴隷カードのクローン.

	protected Transform m_ListRoot;
	protected UIGrid	m_Grid;

	protected virtual void Awake(){
		m_CardList = new CardBase[GlobalDefine.CARD_MAX_NUM];
		m_ListRoot = transform.FindChild("Grid");
		m_Grid = m_ListRoot.GetComponent<UIGrid>();
	}

	protected GlobalDefine.SIDE_TYPE m_SideType;
	public GlobalDefine.SIDE_TYPE sideType{
		get{return m_SideType;}
		set{m_SideType = value;}
	}

	public virtual void cardSetUp(){
		// 手札が存在していた場合破棄しておく.
		deleteCard();

		switch(sideType){
		case GlobalDefine.SIDE_TYPE.EMPEROR:
			createCardEmperor();
			break;
		case GlobalDefine.SIDE_TYPE.SLAVE:
			createCardSlave();
			break;
		default:
			throw new UnityException("not Found CardType : Type > "+sideType);
		}
		// initialize...
	}

	private void deleteCard(){
		for(int i=0; i < m_CardList.Length; ++i){
			if(m_CardList[i] == null){
				continue;
			}
			Destroy(m_CardList[i].gameObject);
			m_CardList[i] = null;
		}
	}

	/// <summary>
	/// 奴隷側のカード作成.
	/// </summary>
	protected virtual void createCardSlave(){
		for(int i=0; i< 4; ++i){
			createClone(m_CitizenClone,i);
		}
		createClone(m_SlaveClone,4);
		reposition();
	}

	/// <summary>
	/// 皇帝側のカード作成.
	/// </summary>
	protected virtual void createCardEmperor(){
		for(int i=0; i< 4; ++i){
			createClone(m_CitizenClone,i);
		}
		createClone(m_EmperorClone,4);
		reposition();
	}

	protected void createClone(CardBase i_Card, int i_Index){
		GameObject clone  = GameObject.Instantiate(i_Card.gameObject) as GameObject;
		clone.transform.parent = m_ListRoot;
		clone.transform.localScale = Vector3.one;
		clone.transform.transform.localPosition = Vector3.zero;
		// カードリストに追加.
		m_CardList[i_Index] = clone.GetComponent<CardBase>();
	}

	public void reposition(){
		m_Grid.Reposition();
	}
}

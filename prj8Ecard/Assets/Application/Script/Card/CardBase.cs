﻿using UnityEngine;
using System.Collections;

public class CardBase : MonoBehaviour {
	protected GlobalDefine.CARD_TYPE m_CardType;
	public GlobalDefine.CARD_TYPE cardType{get{return m_CardType;}}

	protected GameObject m_CardButtonObj;

	protected UISprite m_CardSprite;
	public UISprite cardSprite { get{return m_CardSprite;}  set{cardSprite = value;}}

	protected BoxCollider m_ButtonCollider;
	public BoxCollider buttonCollider{get{return m_ButtonCollider;} set{m_ButtonCollider = value;}}

	protected GameObject m_Root;

	protected string m_CardName;

	protected virtual void Awake(){
		m_CardButtonObj = transform.FindChild("Button").gameObject;
		m_CardSprite = m_CardButtonObj.transform.FindChild("Background").GetComponent<UISprite>();
		buttonCollider = m_CardButtonObj.GetComponent<BoxCollider>();
		m_Root = GameObject.Find("MainScreen");
		m_CardName = "NONE";
	}

	/// <summary>
	/// 拡大アニメーション.
	/// </summary>
	/// <param name="i_Target">I_ target.</param>
	/// <param name="i_Duration">I_ duration.</param>
	public void scaleAnimation(GameObject i_Target, float i_Duration = 0.2f){
		TweenScale.Begin(m_CardSprite.gameObject,i_Duration,i_Target.transform.localScale);
	}

	/// <summary>
	/// カードを伏せる時のアニメーション.
	/// </summary>
	public void setAnimation(GameObject i_Target,float i_Duration = 0.2f){
		TweenRotation tweenHalf = TweenRotation.Begin(this.gameObject,i_Duration,Quaternion.AngleAxis(90, Vector3.up));
		
		tweenHalf.callBackFinish = ()=>{
			// 画像差し替え.
			m_CardSprite.spriteName = "back";
			TweenRotation tweenEnd = TweenRotation.Begin(this.gameObject,i_Duration,Quaternion.AngleAxis(0, Vector3.up));
			tweenEnd.callBackFinish = ()=>{
				scaleAnimation(i_Target);
				moveAnimation(i_Target);
			};
		};
	}

	/// <summary>
	/// カード開示アニメーション.
	/// </summary>
	public virtual void openAnimation(float i_Duration = 0.2f, System.Action callBack = null){
		TweenRotation tweenHalf = TweenRotation.Begin(this.gameObject,i_Duration,Quaternion.AngleAxis(90, Vector3.up));
		
		tweenHalf.callBackFinish = ()=>{
			// 画像差し替え.
			m_CardSprite.spriteName = m_CardName;
			TweenRotation tween = TweenRotation.Begin(this.gameObject,i_Duration,Quaternion.AngleAxis(0, Vector3.up));
			tween.callBackFinish= ()=>{
				callBack();
			};
		};
	}

	/// <summary>
	/// 移動アニメーション.
	/// </summary>
	/// <param name="i_Target">I_ target.</param>
	/// <param name="i_Duration">I_ duration.</param>
	public void moveAnimation(GameObject i_Target,System.Action setCallBack = null ,float i_Duration = 0.2f){

		GameObject field = GameObject.FindGameObjectWithTag("Field");
		gameObject.transform.parent = field.transform;

		// アニメーション登録.
		TweenPosition tween = TweenPosition.Begin(gameObject,0.2f,i_Target.transform.localPosition);
		tween.callBackFinish = ()=>{
			if(setCallBack != null){setCallBack();}
		};
	}
}

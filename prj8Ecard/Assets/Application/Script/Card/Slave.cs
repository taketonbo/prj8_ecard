﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 奴隷カード.
/// </summary>
public class Slave : CardBase {
	protected override void Awake ()
	{
		base.Awake ();
		m_CardName = "slave";
		m_CardType = GlobalDefine.CARD_TYPE.SLAVE;
	}
}

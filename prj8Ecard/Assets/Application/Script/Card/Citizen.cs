﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 市民カード.
/// </summary>
public class Citizen : CardBase {
	protected override void Awake ()
	{
		base.Awake ();
		m_CardName = "citizen";
		m_CardType = GlobalDefine.CARD_TYPE.CITIZEN;
	}


}

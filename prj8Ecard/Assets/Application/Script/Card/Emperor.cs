﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 皇帝カード.
/// </summary>
public class Emperor : CardBase {
	protected override void Awake ()
	{
		base.Awake ();
		m_CardName = "king";
		m_CardType = GlobalDefine.CARD_TYPE.EMPEROR;
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class CardSwipe : DragDropItem {

	public GameObject m_FieldObj;
	private Vector3 m_OldPos;

	private UISprite cardSprite;
	private GameObject cardSpriteObj;

	private SetCardAnimation m_Animation;

	private CardBase m_MyCard;

	private readonly int CARD_DEPTH_OFFSET = 100;

	protected override void Awake(){
		base.Awake();
		m_FieldObj = GameObject.Find("MainScreen").transform.Find("Field").gameObject;
		cardSpriteObj = transform.FindChild("Background").gameObject;
		cardSprite = cardSpriteObj.GetComponent<UISprite>();
		m_MyCard = transform.parent.GetComponent<CardBase>();
		m_Animation = GameObject.Find("FieldSetCard").GetComponent<SetCardAnimation>();
	}

	protected override void Drop ()
	{
		// カードがセットされているなら処理しない.
		if(Field.instance.playerSetCard != null){
			return;
		}
		// Is there a droppable container?
		Collider col = UICamera.lastHit.collider;

		if(col != null){
			//Debug.Log(col.gameObject.name);
			if(col.gameObject == m_Animation.gameObject){
				// カードセット.
				Field.instance.playerSetCard = m_MyCard;
				// コライダoff.
				gameObject.GetComponent<BoxCollider>().enabled = false;
				// アニメーション開始.
				playAnimation(col.gameObject);
			}
		}else{
			// 座標を戻す.
			mTrans.localPosition = m_OldPos;
		}

		// Notify the table of this change
		UpdateTable();
		
		// Make all widgets update their parents
		NGUITools.MarkParentAsChanged(gameObject);
	}

	protected override void OnDrag (Vector2 delta)
	{
		if(Field.instance.playerSetCard != null){
			return;
		}
		base.OnDrag (delta);
	}

	protected override void OnPress (bool isPressed)
	{

		m_OldPos = mTrans.localPosition;

		if(Field.instance.playerSetCard != null){
			return;
		}
		if(isPressed){
			m_Animation.show();
			addDepth(CARD_DEPTH_OFFSET);

		}else{
			m_Animation.hide();
			addDepth(-CARD_DEPTH_OFFSET);
		}
		base.OnPress (isPressed);
	}

	private void playAnimation(GameObject i_Taget){
		m_MyCard.setAnimation(i_Taget);
	}

	private void addDepth(int i_Depth){
		cardSprite.depth += i_Depth;
	}
}

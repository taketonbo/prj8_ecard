﻿using UnityEngine;
using System.Collections.Generic;

public class Loby : Photon.MonoBehaviour {
	private string m_currentRoomName = "";      // 参加しているRoom名.
	public string Player_Name;
	public string Room_Name;
	public bool Player_Ready; //プレイヤーの準備完了
	public bool Enemy_Ready;  //敵の準備完了
	private List<string> Room_List = new List<string>();
	private Room Player_Count;
	public bool Now_Flag;
	// Use this for initialization
	void Start() {
			PhotonNetwork.ConnectUsingSettings ("0.1");
		}
	void Update(){
		if(Player_Ready==true && Enemy_Ready == true){
			Application.LoadLevel("main");
		}
	}
	/**
     * Lobbyに入ったときに呼ばれる.
     */
	void OnJoinedLobby() {
		Debug.Log("ロビーに入ったよ");
	}

	
	/**
     * Lobbyから去ったときに呼ばれる.
     */
	void OnLeftLobby() {
		Debug.Log("ロビーから出たよ");
	}
	
	/**
     * 指定のroomNameのRoomに参加する.
     * roomNameが""の場合は、新しいRoomを生成して参加.
     */
	private void m_JoinRoom(string roomName) {
		PhotonNetwork.CreateRoom(Room_Name,true,true,2);      // Roomを生成/参加.                                     
	}
	
	/**
     * Roomのリストを取得.
     */
	void OnReceivedRoomListUpdate() {

	}

	void OnPhotonCreateRoomFailed() {
		Debug.Log("OnPhotonCreateRoomFailed !");
	}
	
	void OnJoinedRoom() {
		Debug.Log("ルームに入ったよ");
		
		m_currentRoomName = PhotonNetwork.room.name;    // 現在参加しているRoom名を取得.
		Player_Count = PhotonNetwork.room;

	}
	void OnLeftRoom(){
		Debug.Log ("ルームから出たよ");

		m_currentRoomName = "";
		}
	void OnPhotonSerializeView(PhotonStream stream,PhotonMessageInfo info){
		}

	void OnGUI() {
		//////////////////////////////////////////////////
		GUILayout.Label (Player_Ready.ToString());
		GUILayout.Label (Enemy_Ready.ToString());
		GUILayout.Label ("プレイヤー名の入力");
		Player_Name = GUILayout.TextField (Player_Name);
		PhotonNetwork.playerName = Player_Name;
		GUILayout.Label ("ルーム名の入力");
		Room_Name = GUILayout.TextField (Room_Name);
		///////////////////////////////////////////////////
		 if (m_currentRoomName == "") {
		// プッシュするとRoomに参加する.
			if (GUILayout.Button("ルーム作成")) {      
				m_JoinRoom(m_currentRoomName);
			}
		}
		// プッシュするとRoomから出る.
		if (m_currentRoomName != "") {
			if (GUILayout.Button ("ルームから出る")) {
					PhotonNetwork.LeaveRoom ();
			}
			//プレイヤーのフラグをtrueにして
			//通信相手のエネミーフラグをtrueにする
			if(GUILayout.Button("準備完了")){

				Player_Ready = true;
				photonView.RPC("Ready",PhotonTargets.Others);

			}
			//プレイヤーのフラグをfalseにして
			//通信相手のエネミーフラグをfalseにする
			if(GUILayout.Button("解除")){
				Player_Ready = false;
				photonView.RPC("Cancel",PhotonTargets.Others);
			}
			//現在のルーム内の人数
			GUILayout.Label("人数 : " + Player_Count.playerCount);
		}

		GUILayout.Label ("現在のルーム:" + m_currentRoomName);

		// 既存のRoomを取得.
		RoomInfo [] roomInfo = PhotonNetwork.GetRoomList();
		if (roomInfo == null || roomInfo.Length == 0) return;

		// 個々のRoomの名前を表示.
		for (int i = 0; i < roomInfo.Length; i++) {
			if(GUILayout.Button(roomInfo[i].name)){
				PhotonNetwork.JoinRoom(roomInfo[i].name);
			}
		}

	}
	[RPC]
	void Ready(){
		Enemy_Ready = true;
	}
	void Cancel(){
		Enemy_Ready = false;
	}
}

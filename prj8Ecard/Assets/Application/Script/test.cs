﻿using UnityEngine;
using System.Collections;

public class test : Photon.MonoBehaviour {
	private string m_currentRoomName = "";      // 参加しているRoom名.
	private Room Player_Count;
	private bool ready;
	public PhotonView myPV;
	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings ("0.1");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	/**
     * Roomのリストを取得.
     */
	void OnReceivedRoomListUpdate() {
		Debug.Log("OnReceivedRoomListUpdate");

	}

	//ルームを作成
	private void m_JoinRoom(string roomName) {
		if (string.IsNullOrEmpty(roomName)) PhotonNetwork.CreateRoom("room1");      // room1というRoomを生成/参加.
		else PhotonNetwork.JoinRoom(roomName);                                                                 
	}
	void OnJoinedRoom() {
		m_currentRoomName = PhotonNetwork.room.name;
		Player_Count = PhotonNetwork.room;
	}
	void OnGUI(){
		if (GUILayout.Button(" 参加 ")) {      // プッシュするとRoomに参加する.
			m_JoinRoom(m_currentRoomName);
		}

		GUILayout.Label("ルーム名:" + m_currentRoomName);
		if (m_currentRoomName != "") {
			GUILayout.Label("人数 : " + Player_Count.playerCount);
			if(GUILayout.Button("準備完了")){
				photonView.RPC("Ready",PhotonTargets.Others);
			}
			if(GUILayout.Button("解除")){

			}
			GUILayout.Label(ready.ToString());
		}

		// 既存のRoomを取得.
		RoomInfo [] roomInfo = PhotonNetwork.GetRoomList();
		if (roomInfo == null || roomInfo.Length == 0) return;
		// 個々のRoomの名前を表示.
		for (int i = 0; i < roomInfo.Length; i++) {
			//Debug.Log((i).ToString() + " : " + roomInfo[i].name);
			//room[i] =  roomInfo[i].name;
			//Room_List.Add(roomInfo[i].name);
			if(GUILayout.Button(roomInfo[i].name)){
				PhotonNetwork.JoinRoom(roomInfo[i].name);
			}
		}
	}
	[RPC]
	void Ready(){
		ready = true;
	}
}

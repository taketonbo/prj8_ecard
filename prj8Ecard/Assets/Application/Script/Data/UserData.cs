﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UserData {
	public string name;


	/// <summary>
	/// Initializes a new instance of the <see cref="UserData"/> class.
	/// </summary>
	public UserData(){
		name = "player01";
	}
}

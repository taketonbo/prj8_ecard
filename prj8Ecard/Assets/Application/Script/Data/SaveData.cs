﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.IsolatedStorage;
using System;

[SerializeField]
public class SaveData : SingletonMono<SaveData>{
	
	private UserData m_UserData;

	public UserData userData {
		get{return m_UserData;}
		set{ m_UserData = value;}
	}

	protected override void Awake(){
		// iOS環境対策.
		Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER","yes");
		base.Awake();
		//createData ();
		//load ();

	}

	/// <summary>
	/// 進行状況のロード.
	/// </summary>
	public void load(){
		try{
			FileStream fs = new FileStream(GlobalDefine.saveDataPath,
				FileMode.Open,
				FileAccess.Read);
			BinaryFormatter bf = new BinaryFormatter();

			m_UserData		= bf.Deserialize(fs) as UserData;
			fs.Close();

			Debug.Log("Load Success");
		}catch(IOException e){
			Debug.LogWarning (e);
			createData ();
		}catch(IsolatedStorageException e){
			Debug.LogWarning (e);
			createData ();
		}catch(SerializationException e){
			Debug.LogWarning(e);
			createData();
		}
	}

	/// <summary>
	/// 進行状況のセーブ.
	/// </summary>
	public void save(){
		try{
			FileStream fs = new FileStream(GlobalDefine.saveDataPath,
				FileMode.Create,
				FileAccess.Write);

			BinaryFormatter bf = new BinaryFormatter();
			bf.Serialize(fs,m_UserData);

			fs.Close();
		}catch(IOException e){
			Debug.LogError (e);
		}
	}


	/// <summary>
	/// 初期データの作成.
	/// TODO:ここにデバッグするためのデータ等を突っ込んで行く.
	/// </summary>
	public void createData(){
		//各オブジェクトのインスタンス化.
		m_UserData = new UserData();
		save ();
		Debug.LogWarning ("Create New Data");
	}
}

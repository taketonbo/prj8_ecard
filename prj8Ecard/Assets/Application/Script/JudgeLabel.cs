﻿using UnityEngine;
using System.Collections;

public class JudgeLabel : MonoBehaviour {
	public UILabel m_JudgeLabel;
	private FixedInfoMessageData m_MessageData;

	void Start(){
		m_MessageData = FixedData.instance.infoMessageData;
		hide();
	}

	public void showWin(){
		m_JudgeLabel.text = m_MessageData.findFromMessage("MSG_PLAYER_WIN");
		showActive(true);
	}

	public void showLose(){
		m_JudgeLabel.text = m_MessageData.findFromMessage("MSG_PLAYER_LOSE");
		showActive(true);
	}

	public void showDraw(){
		m_JudgeLabel.text = m_MessageData.findFromMessage("MSG_PLAYER_DRAW");
		showActive(true);
	}

	public void hide(){
		showActive(false);
	}

	private void showActive(bool flag){
		m_JudgeLabel.gameObject.SetActive(flag);
	}


}

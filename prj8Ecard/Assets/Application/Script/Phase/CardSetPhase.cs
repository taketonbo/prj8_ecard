﻿using UnityEngine;
using System.Collections;

/// <summary>
/// カードセットフェイズ.
/// </summary>
public class CardSetPhase : MonoBehaviour {

	private Enemy m_Enemy;
	private Player m_Player;

	void Start(){
		m_Player = Field.instance.player;
		m_Enemy = Field.instance.enemy;
	}


	public IEnumerator run(){
		switch(m_Player.sideType){
		case GlobalDefine.SIDE_TYPE.EMPEROR:
			// 先に相手のターン.
			yield return StartCoroutine(enemyTurn());
			yield return new WaitForSeconds(0.5f);
			yield return StartCoroutine(playerTurn());
			yield return new WaitForSeconds(0.5f);
			break;
		case GlobalDefine.SIDE_TYPE.SLAVE:
			// 先にこちらのターン.
			yield return StartCoroutine(playerTurn());
			yield return new WaitForSeconds(0.5f);
			yield return StartCoroutine(enemyTurn());
			yield return new WaitForSeconds(0.5f);
			break;
		}
	}

	private IEnumerator enemyTurn(){
		while(true){
			yield return StartCoroutine(m_Enemy.cpu.selectSetCard());
			break;
		}
		yield return 0;
	}

	private IEnumerator playerTurn(){
		Field field = Field.instance;
		while(field.playerSetCard == null){
			yield return 0;
		}
		yield return 0;
	}
}

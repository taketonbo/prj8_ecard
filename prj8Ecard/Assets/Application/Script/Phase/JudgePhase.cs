﻿using UnityEngine;
using System.Collections;

public class JudgePhase : MonoBehaviour {

	public GameObject m_Dust;

	public JudgeLabel m_JudgeLabel;

	private Field m_Field;

	private Player player {get{return m_Field.player;}}
	private Enemy enemy {get{return m_Field.enemy;}}

	private CardBase playerSetCard {get{return m_Field.playerSetCard;}}
	private CardBase enemySetCard{get{return m_Field.enemeySetCard;}}

	void Start(){
		m_Field = Field.instance;
	}


	public IEnumerator run(){
		switch(player.sideType){
		case GlobalDefine.SIDE_TYPE.EMPEROR:
			judgeEmperor();
			break;
		case GlobalDefine.SIDE_TYPE.SLAVE:
			judgeSlave();
			break;
		}

		// ディレイ.
		yield return new WaitForSeconds(2f);

		m_JudgeLabel.hide();

		Field.instance.enemeySetCard.moveAnimation(m_Dust,deleteEnemyCard,0.5f);
		Field.instance.playerSetCard.moveAnimation(m_Dust,deletePlayerCard,0.5f);

		// ディレイ.
		yield return new WaitForSeconds(0.6f);

		yield return 0;
	}

	private void deletePlayerCard(){
		Destroy(Field.instance.playerSetCard.gameObject);
	}
	private void deleteEnemyCard(){
		Destroy(Field.instance.enemeySetCard.gameObject);
	}

	
	private void judgeEmperor(){
		if(playerSetCard.cardType == GlobalDefine.CARD_TYPE.EMPEROR){
			// 皇帝 vs 市民.
			if(enemySetCard.cardType == GlobalDefine.CARD_TYPE.CITIZEN){
				win();
			}else {
			// 皇帝 vs 奴隷.
				lose ();
			}
		}else{
			// 市民 vs 市民.
			if(enemySetCard.cardType == GlobalDefine.CARD_TYPE.CITIZEN){
				draw();
			}else {
			// 市民 vs 奴隷.
				win ();
			}
		}
	}

	private void judgeSlave(){
		if(playerSetCard.cardType == GlobalDefine.CARD_TYPE.SLAVE){
			// 奴隷 vs 市民.
			if(enemySetCard.cardType == GlobalDefine.CARD_TYPE.CITIZEN){
				lose();
			}else {
			// 奴隷 vs 皇帝.
				win();
			}
		}else{
			// 市民 vs 市民.
			if(enemySetCard.cardType == GlobalDefine.CARD_TYPE.CITIZEN){
				draw();
			}else {
			// 市民 vs 皇帝.
				lose();
			}
		}
	}
	
	/// <summary>
	/// 勝利処理.
	/// </summary>
	private void win(){
		Debug.Log("win");
		m_JudgeLabel.showWin();
		Field.instance.isGameSet = true;
	}

	/// <summary>
	/// 敗北処理..
	/// </summary>
	private void lose(){
		Debug.Log("lose");
		m_JudgeLabel.showLose();
		Field.instance.isGameSet = true;
	}

	/// <summary>
	/// 引き分け処理.
	/// </summary>
	private void draw(){
		Debug.Log("draw");
		m_JudgeLabel.showDraw();
		Field.instance.isGameSet = false;
	}
}

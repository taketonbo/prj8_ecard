﻿using UnityEngine;
using System.Collections;

/// <summary>
/// カード配布フェイズ.
/// </summary>
public class CardDistributePhase : MonoBehaviour {

	private Player m_Player;
	private Enemy m_Enemy;

	/// <summary>
	/// フェイズ処理.
	/// </summary>
	public IEnumerator run(){

		getInstanceImpl();

		m_Player.cardSetUp();
		yield return 0;
		m_Player.reposition();

		m_Enemy.cardSetUp();
		yield return 0;
		m_Enemy.reposition();

		yield return 0;
	}

	private void getInstanceImpl(){
		// インスタンスの取得.
		m_Player = Field.instance.player;
		m_Enemy = Field.instance.enemy;
	}
}

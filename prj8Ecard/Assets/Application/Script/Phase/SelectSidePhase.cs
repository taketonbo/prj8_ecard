﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 皇帝側か奴隷側 選択フェイズコンポーネント.
/// </summary>
public class SelectSidePhase : MonoBehaviour {

	public GameObject m_SideSelectObj;

	private GlobalDefine.SIDE_TYPE m_MySelectSide;
	private GlobalDefine.SIDE_TYPE m_EnemySelectSide;

	private FixedInfoMessageData m_InfoMessage;

	private bool m_IsSelect;
	public bool isSelect{get{return m_IsSelect;}set{m_IsSelect = value;}}

	public UIButton m_EmperorButton;		// 皇帝カードボタン.
	public UIButton m_SlaveButton;			// 奴隷カードボタン.

	// 情報文字ラベル.
	public UILabel m_InfoLabel;

	private Field m_FieldData;

	void Awake(){
		m_InfoMessage = FixedData.instance.infoMessageData;
		m_FieldData = Field.instance;
	}

	void Start(){
		// ボタンのイベント登録.
		m_EmperorButton.callBackPress = selectEmperor;
		m_SlaveButton.callBackPress = selectSlave;
	}

	public IEnumerator run(){
		m_SideSelectObj.SetActive(true);
		setUp();
		while(!isSelect){
			yield return 0;
		}

		m_FieldData.player.sideType = m_MySelectSide;
		// サイド選択をクライアントに送信.
		// send m_MySelectSide...

		// TODO:通信未実装の為、ひとまずこちらで対処.
		// CPUエネミー選択処理.
		m_FieldData.enemy.cpu.selectSide();

		// phase end.
		m_SideSelectObj.SetActive(false);
		yield return null;
	}

	private void setUp(){
		setSelectLabel();
	}

	/// <summary>
	/// 皇帝選択処理.
	/// </summary>
	private void selectEmperor(){
		isSelect = true;
		buttonActive(false);
		m_MySelectSide = GlobalDefine.SIDE_TYPE.EMPEROR;
	}

	/// <summary>
	/// 奴隷選択処理.
	/// </summary>
	private void selectSlave(){
		isSelect = true;
		buttonActive(false);
		m_MySelectSide = GlobalDefine.SIDE_TYPE.SLAVE;
	}


	private void buttonActive(bool flag){
		m_EmperorButton.collider.enabled = flag;
		m_SlaveButton.collider.enabled = flag;
	}

	/// <summary>
	/// 「選択して下さい」ラベルのセット.
	/// </summary>
	private void setSelectLabel(){
		m_InfoLabel.text = m_InfoMessage.findFromMessage("MSG_SELECT_SIDE");
	}

	/// <summary>
	/// 「相手の選択を待っています」ラベルのセット.
	/// </summary>
	private void setEnemyWaitLabel(){
		m_InfoLabel.text = m_InfoMessage.findFromMessage("MSG_ENEMY_WAIT_SELECT");
	}
}

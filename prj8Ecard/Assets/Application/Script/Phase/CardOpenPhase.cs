﻿using UnityEngine;
using System.Collections;

public class CardOpenPhase : MonoBehaviour {

	private int m_Count;

	public IEnumerator run(){
		initializeCount();

		while(true){
			Field.instance.playerSetCard.openAnimation(0.5f,addCount);
			Field.instance.enemeySetCard.openAnimation(0.5f,addCount);
			break;
		}

		// アニメーションが終了するまで待機.
		while(m_Count < 2){
			yield return null;
		}
		yield return null;
	}

	private void addCount(){
		m_Count++;
	}
	private void initializeCount(){
		m_Count = 0;
	}
}
